<?php

	namespace Itul\PdfData;

	/*
		THIS CLASS HAS SOME COMMAND LINE REQUIREMENTS
		pdftk: https://askubuntu.com/questions/1028522/how-can-i-install-pdftk-in-ubuntu-18-04-and-later
		imagick: https://www.serverlab.ca/tutorials/linux/administration-linux/how-to-install-imagemagick-for-php-on-ubuntu-18-04/
		inkscape: https://wiki.inkscape.org/wiki/index.php/Installing_Inkscape
	*/

	class PdfData {

		public static $instance;

		public function __construct($pdf_file){
			\Itul\PdfData\PdfData::$instance 	= $this;
			$this->_instanceId 					= uniqid('pdf_data_helper_', true);
			$this->_parsed 						= false;
			$this->_catalog 					= null;
			$this->_objects 					= [];
			$this->_pdf_file 					= $pdf_file;
			$this->_streams 					= [];
			$this->_fieldPrefix 				= "";
			$this->_groupFieldWrappers 			= [];
			$this->_fieldWrap 					= null;
			$this->_rasterize 					= true;
			$this->_rasterFormat 				= 'png';
			$this->_svgText 					= false;
			$this->_main_objects 				= [];
			$this->_parsedImages 				= [];
			$this->_pageStylesRendered 			= false;
			$this->_debug 						= false;
			$this->_renderOptions 				= [
				'placeholder' 		=> false,
				'showButtons' 		=> false,
				'density' 			=> 300,
				'defaultFontSize' 	=> 9,
				'includeImage' 		=> true,
			];

			$this->_defaultPageStyles = "<style>
	/*--------------/ PAGES LIKE ACROBBAT  /--------------*/	
	.page { border: 1px solid #ccc; border-radius: 5px; overflow: hidden; margin-top: 20px; margin-bottom: 20px; margin-left:auto; margin-right:auto; box-shadow: rgb(0 0 0 / 22%) 0 0 10px; transform: scale(1); transform-origin:top left; position:relative;}

	.page > img {position:absolute; top:0; left:0; /*width:100%; height:100%*/}

	/*--------------/ DEFAULT FOR FORM FIELDS  /--------------*/	
	.page .pdf-form-field {position:absolute; margin:0pt; padding:0pt; font-size:{$this->_renderOptions['defaultFontSize']}pt;}

	/*--------------/ CHECKBOXES LIKE ACROBBAT  /--------------*/		
	.page .custom-checkbox {display: block; position: absolute; margin:0pt; padding:0pt; cursor: pointer; font-size: 22px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;} /* Customize the label (the container) */
	.page .custom-checkbox input { position: absolute; opacity: 0; cursor: pointer; height: 0; width: 0;} /* Hide the browser's default checkbox */
	.page .checkmark { position: absolute; top: 0; left: 0; height: 100%; width: 100%; } /* Create a custom checkbox wraping element */	
	.page .checkmark:after { content: \"\"; position: absolute; background-image: url(\"data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' width='263' height='280'><path d='M 30,180 90,240 240,30' style='stroke:%23333; stroke-width:50; fill:none' /></svg>\"); background-size: 0px; background-position: center center; background-repeat: no-repeat; width: 100%; height: 100%; transition: all .22s;} /* Create the checkmark/indicator (hidden when not checked) */
	.page .custom-checkbox input:checked ~ .checkmark:after { background-size: 70%; } /* Show the checkmark when checked */
	.page .custom-checkbox:hover {background: #7575ef4a;}

	/*--------------/ TEXT FIELDS LIKE ACROBBAT  /--------------*/	
	.pdf-form-field.pdf-form-field-textarea, .pdf-form-field.pdf-form-field-text {border:none;background:#7575ef4a;}
	.pdf-form-field.pdf-form-field-textarea:hover, .pdf-form-field.pdf-form-field-text:hover, .pdf-form-field.pdf-form-field-textarea:focus, .pdf-form-field.pdf-form-field-text:focus { border: none; background: #7575ef4a; }
	.pdf-form-field.pdf-form-field-textarea:focus, .pdf-form-field.pdf-form-field-text:focus { outline:none; }
    .pdf-form-field.pdf-form-field-textarea {resize:none}
</style>";
			return $this;
		}

		public static function of($pdf_file){
			return new \Itul\PdfData\PdfData($pdf_file);
		}

		public function renderOptions($key = null, $val = null){
			if(is_null($this->_renderOptions)) $this->_renderOptions = PdfData::$instance->_renderOptions;
			if(!is_null($key) && is_array($key)) foreach($key as $k => $v) $this->renderOptions($k, $v);
			elseif(!is_null($key)) $this->_renderOptions[$key] = $val;
			return $this;
		}

		//UNCOMPRESS THE INPUT PDF
		public function uncompress(){
			$file_name 	= sys_get_temp_dir().'/'.uniqid('uncompressed_pdf_', true).'.pdf';
			exec("pdftk {$this->_pdf_file} output {$file_name} uncompress");
			return $file_name;
		}

		public function load_resource_array($array = []){
			$res = new PDF_Data_Collection;
			foreach($array as $k => $v) $res->push($v->get());

			return $res;
		}

		public function rasterize($val = true){
			if(in_array($val, ['jpg','png'])){
				$this->_rasterFormat = $val;
				$val = true;
			}
			$this->_rasterize = $val;
			return $this;
		}

		public function parseImages(){

			if(!$this->_renderOptions['includeImage']) return $this->_parsedImages;

			//CHECK IF IMAGES HAVE BEEN PARSED YET
			if(empty($this->_parsedImages)){

				//CHECK IF WE WANT VECTOR BACKGROUNDS
				if(!$this->_rasterize){

					//GET IMAGES AS SVG
					$prefix 			= uniqid('pdf_burst', true);
					$temp_pdf_names 	= sys_get_temp_dir().'/'.$prefix.'%04d.pdf';
					$cmd 				= "pdftk {$this->_pdf_file} burst output {$temp_pdf_names}";
					exec($cmd);
					$temp_pdfs 			= glob(dirname($temp_pdf_names).'/'.$prefix.'*.pdf');
					$images 			= [];

					//LOOP THROUGH THE SPLIT PDFS AND CONVERT THEM TO SVG
					foreach($temp_pdfs as $inputFile){
						$prefix = uniqid('pdf_to_svg_', true);
						$outputFile = sys_get_temp_dir().'/'.$prefix.'.svg';

						if(!$this->_svgText){
							//$target = (new \Itul\PdfTools\PdfTools)->_randomName('svg');
							$out = [];
							$ret;
							exec("~/conda/bin/pdf2svg {$tempCopy} {$outputFile} 2>&1", $out, $ret);
							$images[] = str_replace('<?xml version="1.0" encoding="UTF-8"?>'."\n", '', file_get_contents($outputFile));
						}
						
						else{
							$cmd = "inkscape --without-gui --file={$inputFile} --export-plain-svg={$outputFile}";
							exec($cmd);
							$images[] = str_replace('<?xml version="1.0" encoding="UTF-8" standalone="no"?>'."\n", '', file_get_contents($outputFile));
						}

						
					}
				}

				//CONVERT THE PDF INTO IMAGES
				else{	
					$img_prefix 		= uniqid('pdf_to_image_', true);
					$temp_img_names 	= sys_get_temp_dir().'/'.$img_prefix.'%04d.'.$this->_rasterFormat;
					exec("convert -colorspace RGB -interlace none -density {$this->_renderOptions['density']}x{$this->_renderOptions['density']} {$this->_pdf_file} -quality 100 {$temp_img_names}");
					$images 			= glob(dirname($temp_img_names).'/'.$img_prefix.'*.'.\Itul\PdfData\PdfData::$instance->_rasterFormat);
				}

				//SET THE PARSED IMAGE ARRAY
				$this->_parsedImages = $images;
			}

			//SEND BACK THE PARSED IMAGE ARRAY
			return $this->_parsedImages;
		}

		public function htmlData(){

			//INIT THE PAGES ARRAY
			$pages 				= [];
			$pageObjects 		= new PDF_Data_Collection;

			$this->parseImages();			

			//LOOP THROUGH THE PARSED PAGES
			foreach($this->parse()->pages() as $k => $page){

				if(!$this->_renderOptions['includeImage']){
					$data = (object)['image' => false];
				}
				else{
					//INIT PAGE DATA WITH IMAGE
					$data = !$this->_rasterize ? (object)['image' => $this->_parsedImages[$k]] : (object)['image' => 'data:image/'.\Itul\PdfData\PdfData::$instance->_rasterFormat.';base64,'.base64_encode(file_get_contents($this->_parsedImages[$k]))];
				}

				
				
				//ADD PAGE DATA TO OBJECT
				foreach($page->htmlData() as $pK => $pV) $data->$pK = $pV;

				//ADD OBJECT TO PAGES
				$pages[] = $data;
				$pageObjects->push($page);
			} 

			//SEND BACK THE PAGES AS AN OBJECT
			return (object)['pages' => (object)$pages, 'pageObjects' => $pageObjects];
		}

		public function prefixFieldName($val = ''){
			$this->_fieldPrefix = $val;
			return $this;
		}

		public function _generateFieldName($name){
			if($this->_fieldPrefix != '' && substr($this->_fieldPrefix, -1) == '[') return $this->_fieldPrefix.$name.']';

			return $this->_fieldPrefix.$name;
		}

		public function groupFieldWrap($arr = []){
			$this->_groupFieldWrappers = $arr;
			return $this;
		}

		public function fieldWrap($pattern){
			$this->_fieldWrap = $pattern;
			return $this;
		}

		public function _styleFontSize($field){
			//$fontSize = $field->dimensions->height;
			$fontSize = @$field->fontSize ? $field->fontSize : $field->dimensions->height;
			if($fontSize > $this->_renderOptions['defaultFontSize']) $fontSize = $this->_renderOptions['defaultFontSize'];
			$fontSize = $field->dimensions->height < $this->_renderOptions['defaultFontSize'] ? $fontSize*0.8 : $fontSize;
			return $fontSize != $this->_renderOptions['defaultFontSize'] ? 'font-size: '.$fontSize.'pt;' : ' ';
			//return 'font-size: '.($field->dimensions->height < 9 ? $fontSize*0.8 : $fontSize ).'pt; ';
		}

		public function htmlPages($prefix = null, $values = []){

			$pages = [];

			foreach($this->htmlData()->pageObjects as $k => $page) $pages[$k] = $page;

			//SEND BACK A COLLECTION
			return new PDF_Data_Collection($pages);
		}

		public function dumpFields(){

			//SET A TEMP FILE NAME
			$file_name = sys_get_temp_dir().'/'.uniqid('pdf_data_fields_', true);

			//GET THE FIELD NAMES FROM THE 
			exec("pdftk {$this->_pdf_file} dump_data_fields_utf8 output {$file_name}");

			//INIT THE FIELDS ARRAY
			$fields = [];

			//LOOP THROUGH EACH FIELD AND PARSE INTO AN OBJECT
			foreach(array_filter(explode("---", file_get_contents($file_name))) as $field_data){
				
				//INIT A NEW OBJECT
				$new_data = (object)[];

				//LOOP THROUGH THE FIELD DATA AND ADD TO THE NEW OBJECT
				foreach(array_filter(explode("\n", $field_data)) as $line){
					$parts 				= explode(": ", $line);
					$name 				= substr($parts[0], 5);
					$new_data->$name 	= $parts[1];
				}

				//APPEND THE NEW OBJECT TO THE FIELDS ARRAY
				$fields[$new_data->Name] = $new_data;
			}

			//SEND BACK THE FIELDS
			return $fields;
		}

		public function lineCheckObjName($line){

			$res = false;

			if(preg_match('/(([\d]+) ([\d]+)) obj/', $line, $matches)){
				
				$res 		= new PDF_Data_Main_Object($matches[2], $matches[3]);
				$newLine 	= trim(str_replace($matches[0], '', $line));
				if(!empty($newLine)) $res->addBufferLine($newLine);
				return $res;
			} 

			return false;
		}

		public function parse(){

			//JUST RETURN IF ALREADY PARSED
			if($this->_parsed) return $this;			

			//UNCOMPRESS THE PDF
			$uncompressed 	= $this->uncompress();

			//INIT PARSING VALUES		
			$handle 		= fopen($uncompressed, 'r');
			$buffer 		= "";
			$stream 		= false;
			$streamBuffer 	= "";
			$obj_type 		= false;
			$full_buffer 	= "";
			$obj_open 		= false;

			//LOOP THROUGH EACH LINE OF THE UNCOMPRESSED PDF
			while(!feof($handle)){
				
				//GET THE LINE
				$line 			= fgets($handle);

				//ADD THE LINE TO THE FULL BUFFER
				$full_buffer 	.= $line;

				//TURN ON STREAM BUFFER
				if(trim($line) == 'stream'){
					$stream = true;
					$streamBuffer = "";
					continue;	
				}

				//APPEND TO THE STREAM BUFFFER
				if($stream === true){

					if(trim($line) == 'endstream'){
						$stream 					= false;
						$streamId 					= uniqid('pdf_data_stream_', true);
						$this->_streams[$streamId] 	= $streamBuffer;
						$streamBuffer 				= "";
						$buffer 					.= "/Stream ".$streamId;
						continue;
					}
					
					$streamBuffer .= $line;

					continue;
				}

				//SPLIT THE LINE INTO WORDS
				$line_parts 	= explode(" ", trim($line));

				//CHECK IF WE ARE OPENING AN OBJECT
				if(isset($line_parts[2]) && $line_parts[2] == 'obj'){

					//GET THE OBJECT NAME
					$obj_name 	= $line_parts[0].' '.$line_parts[1];
					$buffer 	= "";

					if(count($line_parts) > 3){
						unset($line_parts[0]);
						unset($line_parts[1]);
						unset($line_parts[2]);
						$buffer .= implode(" ", $line_parts)."\n";
					}
				}
				elseif(trim($line) == 'endobj'){

					//START THE OBJECT
					$obj = new PDF_Data_Object($obj_name, $buffer, $this->_instanceId);

					//ADD THE OBJECT TO THE OBJECT LIST
					$this->_objects[$obj_name] = $obj->transform();

					//CHECK FOR A CATALOG OBJECT
					if(get_class($this->_objects[$obj_name]) == 'Itul\PdfData\PDF_Data_Catalog') $this->_catalog = $this->_objects[$obj_name];
				}
				else{
					$buffer .= $line;
				}
			}

			fclose($handle);
			ksort($this->_main_objects);

			$this->_parsed = true;

			return $this;
		}

		public function pages(){

			//MAKE SURE THIS IS PARSED
			$this->parse();

			//CHECK IF THIS HAS A CATALOG
			if(is_null($this->_catalog)){

				//START A NEW COLLECTION
				$res = new \Itul\PdfData\PDF_Data_Collection;

				//FIND THE PAGES
				foreach($this->_objects as $obj) if(get_class($obj) == 'Itul\PdfData\PDF_Data_Page') $res->push($obj);

				//RETURN THE PAGES
				return $res;
			}

			return $this->load_resource_array($this->_catalog->Pages->get()->Kids);
		}
	}

	class PDF_Data_Dictionary implements \ArrayAccess{

		private $_data = [];

		public function __construct($data, $debug = false){
			$this->parse($data, $debug);			
		}

		public function offsetSet($offset, $value) {
        	if(is_null($offset)) {
	            $this->_data[] = $value;
	        } else {
	            $this->_data[$offset] = $value;
	        }
	    }

	    public function offsetExists($offset) {
	        return isset($this->_data[$offset]);
	    }

	    public function offsetUnset($offset) {
	        unset($this->_data[$offset]);
	    }

	    public function offsetGet($offset) {
	        return isset($this->_data[$offset]) ? $this->_data[$offset] : null;
	    }

		public function &__get($name){

			$res = null;
			if(array_key_exists($name, $this->_data)){
				$res = $this->_data[$name];

				if(is_object($res) && get_class($res) == 'Itul\PdfData\PDF_Data_Object_Proxy') $res = $res->get();
			} 

			return $res;
		}

		public function __set($name, $value){
			return $this->_data[$name] = $value;
		}

		public function __isset($name){
			return isset($this->_data[$name]);
		}

		public function __unset($name){
			unset($this->_data[$name]);
		}

		public function parse($buffer, $debug = false){

			$breakReplace 	= "_-_newLineCharacter_-_";
			$subPattern 	= '/<<((?:.*\r?\n?)*)>>/';
			$newSub 		= '/(\/(\w*)( ?)(\n?)<<((?:[^<>]|(?R))*)>>)/';
			$subPattern 	= '/((\/)(.\w)(?:.*\n?))?(<<((?:.*\r?\n?)*)>>)/';

			//REPLACE NEW LINE CHRACTERS
			$buffer = str_replace($breakReplace, "\n", $buffer);

			//COLLAPSE SUB DICTIONARIES TO SINGLE LINE
			if(preg_match_all($newSub, $buffer, $matches)) foreach($matches[0] as $match) $buffer = str_replace($match, str_replace("\n", $breakReplace, $match), $buffer);

			//SPLIT THE LINES
			$lines = explode("\n", trim($buffer));

			foreach($lines as $lineKey => $line){

				if(substr(trim($line), 0, 1) === '/'){

					preg_match('/\/(.+?) /', $line, $matches);

					$key 	= $matches[1];
					$value 	= trim(substr(trim($line), strlen($matches[0])));

					if(preg_match($subPattern, $value, $subMatches)){
						$this->$key = new PDF_Data_Dictionary(trim(str_replace($breakReplace, "\n", $subMatches[5])));
					}

					elseif(substr($value, 0, 1) == '[' && substr($value, -1) == ']'){
						$this->$key = new PDF_Data_Array(substr($value, 1, -1));
					}
					elseif(preg_match('/(?:((\d+) \d+) R)/', $value, $subMatches)){
						$this->$key = new PDF_Data_Object_Proxy($subMatches[2]);
					}
					elseif(substr($value, 0, 1) == '(' && substr($value, -1) == ')'){
						$this->$key = substr($value, 1, -1);
					}
					else{
						$this->$key = $value;
					}
				}
			}

			return $this;
		}
	}

	class PDF_Data_Array implements \ArrayAccess,\Iterator,\Countable{

		protected $_data = [];
		protected $_position = 0;

		public function __construct($data, $debug = false){		
			$this->parse($data, $debug);
		}

		public function transform(){
			return $this;
		}

		public function offsetSet($offset, $value) {
        	if(is_null($offset)) {
	            $this->_data[] = $value;
	        } else {
	            $this->_data[$offset] = $value;
	        }
	    }

	    public function offsetExists($offset) {
	        return isset($this->_data[$offset]);
	    }

	    public function offsetUnset($offset) {
	        unset($this->_data[$offset]);
	    }

	    public function offsetGet($offset) {
	        return isset($this->_data[$offset]) ? $this->_data[$offset] : null;
	    }

	    public function rewind() {
	        $this->_position = 0;
	    }

	    public function current() {
	        return $this->_data[$this->_position];
	    }

	    public function key() {
	        return $this->_position;
	    }

	    public function next() {
	        ++$this->_position;
	    }

	    public function valid() {
	        return isset($this->_data[$this->_position]);
	    }

	    public function nth($number){
	    	return $this->_data[$number];
	    }

	    public function count(){
	    	return count($this->_data);
	    }

		public function parse($data, $debug = false){
			
			if(preg_match_all('/\[((?>[^\[\]]|(?R))*)\]/', $data, $matches)){

			}

			//COLLAPSE OBJECT REFS
			if(preg_match_all('/(?:(\d+ \d+) R)/', $data, $matches)) foreach($matches[0] as $match) $data = str_replace($match, str_replace(' ', '_', $match), $data);

			//SPLIT THE ARRAY INTO PARTS
			$parts = explode(' ', $data);

			//LOOP THROUGH THE PARTS
			foreach($parts as $k => $part){

				//CHECK IF STRING
				if(substr($part, 0, 1) == '(' && substr($part, -1) == ')'){
					$part = substr($part, 1, -1);
				}

				//CHECK IF OBJECT REF
				elseif(preg_match_all('/(?:(\d+_\d+)_R)/', $part, $matches)){
					foreach($matches[0] as $match) $part = str_replace($match, str_replace('_', ' ', $match), $part);
					preg_match('/(?:((\d+) \d+) R)/', $part, $subMatches);

					$part = new PDF_Data_Object_Proxy($subMatches[2]);
				}

				$parts[$k] = $part;
			}

			$this->_data = $parts;

			return $this;
		}
	}

	class PDF_Data_Object_Proxy{

		public function __construct($id){
			$this->_id = $id;
		}

		public function get(){
			return PdfData::$instance->_main_objects[$this->_id];
		}
	}

	class PDF_Data_Main_Object implements \ArrayAccess{

		public 		$_id;
		public 		$_generation;
		private 	$_buffer = "";
		public 		$dictionary;
		public 		$stream;

		public function __construct($id, $generation = null){

			if(is_object($id)){
				foreach($id as $k => $v) $this->$k = $v;
			}
			else{
				$this->_id 			= $id;
				$this->_generation 	= $generation;
			}			
		}

		public function toArray(){
			$res = [];
			foreach($this as $k => $v) $res[$k] = $v;
			return $res;
		}

		public function transform(){

			if(get_class($this) == 'Itul\PdfData\PDF_Data_Main_Object'){
				$className = '\Itul\PdfData\PdfData'.substr($this->Type, 1);
				if(!is_null($this->Type) && class_exists($className)) return new $className($this);
			}			

			return $this;
		}

		public function offsetSet($offset, $value) {
        	if(is_null($offset)) {
	            $this->_data[] = $value;
	        } else {
	            $this->_data[$offset] = $value;
	        }
	    }

	    public function offsetExists($offset) {
	        return isset($this->_data[$offset]);
	    }

	    public function offsetUnset($offset) {
	        unset($this->_data[$offset]);
	    }

	    public function offsetGet($offset) {
	        return isset($this->_data[$offset]) ? $this->_data[$offset] : null;
	    }

		public function &__get($name){
			if(isset($this->dictionary->$name)){
				return $this->dictionary->$name;
			}
		}

		public function &__set($name, $value){
			return $this->dictionary[$name] = $value;
		}

		public function __isset($name){
			return isset($this->dictionary[$name]);
		}

		public function __unset($name){
			unset($this->dictionary[$name]);
		}

		public function addBufferLine($line){
			$this->_buffer .= $line;

			return $this;
		}

		public function parse(){

			$buffer = $this->_buffer;
			$this->_buffer = null;

			//CHECK FOR STREAM
			if(preg_match('/stream((?:.*\r?\n?)*)endstream/', $buffer, $matches)) $buffer = str_replace($matches[0], '', $buffer);
			
			//BUILD THE DICTIONARY
			if(preg_match('/((\/)(.\w)(?:.*\n?))?(<<((?:.*\r?\n?)*)>>)/', $buffer, $matches)) $this->dictionary = new PDF_Data_Dictionary($matches[5]);

			//SEND BACK THE TRANSFORMED OBJECT
			return $this->transform();
		}
	}

	class PDF_DataPage extends PDF_Data_Main_Object{

		public function __construct($data){
			parent::__construct($data);
		}
	}

	class PDF_DataAnnot extends PDF_Data_Main_Object{

		public function __construct($data){
			parent::__construct($data);
		}
	}

	class PDF_Data_Object {

		protected $_name;
		protected $_instanceId;
		protected $_rawData;

		public function __construct($name, $data, $instanceId){

			$this->_name 		= $name;
			$this->_instanceId 	= $instanceId;
			if(PdfData::$instance->_debug) $this->_rawData = $data;
			$this->parse($data);
		}

		public function instanceId(){
			return $this->_instanceId;
		}

		public function toArray(){
			$arr = [];
			foreach($this as $k => $v) $arr[$k] = $v;
			return $arr;
		}

		public function transform(){

			if(isset($this->Stream)){
				return new PDF_Data_Stream_Resource($this->toArray());
			}

			if(isset($this->Type)){
				$class_name = '\Itul\PdfData\PDF_Data_'.$this->Type;

				if(class_exists($class_name)){
					return new $class_name($this->toArray());
				}
			}

			return $this;
		}

		public function parse($rawData){

			$lines = array_filter(explode("\n", $rawData));

			if(count($lines) === 1){
				if(substr(trim($lines[0]), 0, 1) == '[' && substr(trim($lines[0]), -1) == ']'){
					$this->array = $this->parseLineArray($lines[0]);
					return $this;
				}
			}

			$depth 			= 0;
			$arr 			= [];
			$depthKeys 		= [];
			$streaming 		= false;
			$streamBuffer	= "";

			if(trim($lines[0]) == '<<'){

				$obj_depth = 0;
				
				foreach($lines as $k => $line){
					if($k === 0){
						unset($lines[$k]);
						continue;	
					} 

					if(trim($line) == '<<'){
						$obj_depth++;
						continue;
					}

					if(trim($line) == '>>'){
						if($obj_depth === 0){
							unset($lines[$k]);
							break;
						}
						$obj_depth--;
					}
				}
			}

			foreach($lines as $k => $line){

				if(in_array(trim($line), ['<<','>>'])){
					if(trim($line) == '<<'){
						$depth++;
						if($depth >= 1) $depthKeys[$depth] = $key;
					}
					elseif(trim($line) == '>>'){
						$depth--;
						if($depth >= 1) foreach($depthKeys as $dk => $dv) if($dk > $depth) unset($depthKeys[$dk]);
					}					
				}
				else{
					$line_parts = explode(" ", trim($line));
					$key 		= substr(trim($line_parts[0]), 1);
					unset($line_parts[0]);
					$value 		= implode(" ", $line_parts);
					$value 		= (empty(trim($value)) && array_key_exists(($k+1), $lines) && trim($lines[$k+1]) == '<<') ? (object)[] : $this->parse_line($value);

					$arr[] 		= [
						'key' 			=> $key,
						'value' 		=> $value,
						'depth' 		=> $depth,
						//'line_key' 	=> $k,
						'depth_keys' 	=> $depthKeys,
						//'line' 			=> $line
					];
				}
			}

			foreach($arr as $k => $d){
				$key = $d['key'];
				if($d['depth'] < 1){					
					$this->$key = $d['value'];
				}
				else{
					$depthKeys = $d['depth_keys'];
					$obj = $this;
					for($x = 1; $x <= $d['depth']; $x++)  $obj = $obj->{$depthKeys[$x]};
					$obj->{$d['key']} = $d['value'];
				}
			}
			return $this;

		}

		public function parse_line($line){

			$line = trim($line);
			
			if(substr($line, 0, 1) == '[' && substr($line, -1) == ']'){
				return $this->parseLineArray($line);
			}
			elseif(substr($line, -1) == 'R' && substr_count($line, " ") >= 2){

				$line_parts = explode(" ", $line);
				$chunks = array_chunk($line_parts, 3);
				$resources = new PDF_Data_Collection;

				foreach($chunks as $k => $chunk){
					unset($chunk[2]);
					$resources->push(new PDF_Data_Resource(implode(" ", $chunk), $this->_instanceId));
					//$resources->push(implode(" ", $chunk));
				}

				return $resources->count() === 1 ? $resources->first() : $resources->to_array();
			}
			elseif(substr($line, 0, 1) === '/' && str_word_count($line) === 1){
				$line = substr($line, 1);
			}
			elseif(substr($line, 0, 1) === '(' && substr($line, -1) === ')'){
				$line = substr($line, 1, -1);
			}

			return $line;
		}

		public function parseLineArray($line){
			$line = substr($line, 1, -1);

			$parts = explode(" ", $line);

			$chunks = array_chunk($parts, 3);

			$new_parts = [];

			foreach($chunks as $chunk){
				if(isset($chunk[2]) && $chunk[2] == 'R'){
					$new_parts[] = $this->parse_line(implode(" ", $chunk));
				}
				else{
					foreach($chunk as $part){
						$new_parts[] = $part;
					}
				}
			}

			return $new_parts;
		}
	}

	

	class PDF_Data_Resource{

		protected $_resourceName; 
		protected $_instanceId;

		public function __construct($name, $instanceId){
			$this->_resourceName = $name;
			$this->_instanceId = $instanceId;
		}

		public function instanceId(){
			return $this->_instanceId;
		}

		public function get(){
			return PdfData::$instance->_objects[$this->_resourceName];
		}
	}

	class PDF_Data_Type_Base {

		protected $_name;

		public function __construct($data){
			foreach($data as $k => $v) $this->$k = $v;
		}

		public function load_resource_array($array = []){
			$res = new PDF_Data_Collection;

			foreach($array as $k => $v){
				$res->push($v->get());
			}

			return $res;
		}

	}

	class PDF_Data_Stream_Resource extends PDF_Data_Type_Base{

		public function __construct($data){

			parent::__construct($data);
		}

		public function getStream(PDF_Data_Page $page = null){
			return new PDF_Data_Stream(PdfData::$instance->_streams[$this->Stream], $page);
		}
	}

	class PDF_Data_Stream{

		protected $_rawData;
		protected $_page;
		public $texts = [];

		public function __construct($rawData, $page = null){
			$this->_rawData = $rawData;
			$this->_page = $page;
			$this->parse();
		}

		public function parse(){

		

			$lines = explode("\n", $this->_rawData);

			$text = false;
			$textBuffer = "";
			foreach($lines as $k => $line){

				if($k == 0){
					$name = trim($line);
				}
				if($line == "BT"){
					$text = true;
					continue;
				}

				if($text){
					
					if($line == 'ET'){
						$text = false;

						$this->texts[$name] = new PDF_Data_Stream_Text($textBuffer, $this->_page);
						continue;
					}

					$textBuffer .= $line."\n";


				}
			}

			if($text){
				$text = false;
				$textBuffer .= $line."\n";
				$this->texts[$name] = new PDF_Data_Stream_Text($textBuffer, $this->_page);
			}

			return $this;

		}
	}

	class PDF_Data_Stream_Text {
		
		private $_rawData;
		private $_page;
		public $text;

		public function __construct($rawData, $page = null){
			$this->_rawData = trim($rawData);
			$this->_page = $page;
			$this->parse();
		}

		/*
		@TODO Text parsing for non field objects needs to be resolved.
		*/
		public function parse(){
			return $this;		

			/*
			$parts = explode("\n", $this->_rawData);

			$out = [];

			foreach($parts as $k => $part){
				if(substr(trim($part), -2) == 'TJ'){
					preg_match_all('/(:?\((.*?)\))/', $part, $m);
					$str = '';
					foreach($m[2] as $s) $str .= $s;
					$out[] = $str;
				}
			}			
			if(isset($parts[2])){
				$sub_parts = explode(" ", $parts[2]);
				$sub_parts_parsed = [];
				$temp_part = [];
				foreach($sub_parts as $k => $v){
					if(is_numeric($v)){
						$temp_part[] = $v;
					}
					else{
						$sub_parts_parsed[$v] = $temp_part;
						$temp_part = [];
					}
				}

				foreach($sub_parts_parsed as $k => $v){
					$this->$k = $v;
				}

				
				if(substr($parts[3], -3) == ')Tj'){
					$this->text = substr($parts[3], 1, -3);
				}

				return $this;
			}

			return $this;
			*/
		}

		public function htmlData(){

			$pageDimensions = $this->_page->dimensions();
			
			return (object)[
				'text' 			=> $this->text,
				'dimensions' 	=> (object)[
					'left' 		=> isset($this->Tm[4]) ? $this->Tm[4] : 0,
					'top' 		=> $pageDimensions->height-(isset($this->Tm[5]) ? $this->Tm[5] :0),
				],
			];
		}


	}

	class PDF_Data_Annot extends PDF_Data_Type_Base{

		public function __construct($data){
			parent::__construct($data);
		}

		public function page(){
			if(!$this->P) return new PDF_Data_Page;
			return $this->P->get();
		}

		public function dimensions($multiplier = 1, $round = false, $topOffset = false){

			$pageDimensions = $this->page()->dimensions();

			if(is_object($this->Rect)) return (object)[];

			return (object)[
				'top' 		=> ($pageDimensions->height-$this->Rect[1])-($this->Rect[3]-$this->Rect[1]),
				'left' 		=> floatval($this->Rect[0]),
				'width' 	=> $this->Rect[2]-$this->Rect[0],
				'height' 	=> $this->Rect[3]-$this->Rect[1], 
				'bottom' 	=> floatval($this->Rect[3]),
				'right' 	=> floatval($this->Rect[1]),
			];
		}

		private function _parseTextFieldOptions($typeCheck){

			/*
				BIT POSITIONS ARE REFERENCED AT:
				https://opensource.adobe.com/dc-acrobat-sdk-docs/pdfstandards/PDF32000_2008.pdf
				ON PAGE 451
			*/

			$bitPositions = [
				'multiline' 	=> 13,
				'password' 		=> 14,
				'fileSelect' 	=> 21,
				'spellCheck' 	=> 23,
				'disableScroll' => 24,
				'comb' 			=> 25,
				'richText' 		=> 26,
			];

			return substr(decbin($this->Ff), -$bitPositions[$typeCheck], 1) ? true : false;
		}

		public function _parseFontSize(){
			if(isset($this->DA)){
				$parts = explode(' ', $this->DA);
				if(isset($parts[1])){
					return $parts[1];
				}
			}
			return PdfData::$instance->_renderOptions['defaultFontSize'];
		}

		public function htmlData($multiplier = 1){

			$value = null;

			if(isset($this->FT) && $this->FT == 'Btn') foreach($this->AP->D as $value => $d) break;

			$type = @$this->FT;

			if(!$type){

				if($this->Parent){
					$parent = $this->Parent->get();
					$this->T = $parent->T;
					$this->FT = $parent->FT;
					$this->Ff = @$parent->Ff;
					$type = $this->FT;
				}				
			}

			$scrollable = false;

			if($type == 'Tx'){
				$type 		= $this->_parseTextFieldOptions('multiline') ? 'textarea' : 'text'; //CHECK IF TEXT FIELD OR TEXTAREA
				$scrollable = $this->_parseTextFieldOptions('disableScroll') ? false : true; 	//CHECK IF SCROLLABLE
				$type 		= $this->_parseTextFieldOptions('password') ? 'password' : $type; 	//CHECK FOR PASSWORD
			} 
			if($type == 'Btn') $type = 'checkbox';
			if($type == 'checkbox' && isset($this->H) && $this->H == 'P') $type = 'button';

			$dimensions = $this->dimensions();

			$d = (object)[];
			foreach($this as $k => $v){
				$v = $k != 'TU' ? json_decode(json_encode($v)) : ltrim($v);
				$d->$k = $v;
			}

			return (object)[
				'name' 			=> $this->T,
				'type' 			=> $type,
				'value' 		=> $value,
				'scrollable' 	=> $scrollable,
				'MaxLen' 		=> @$this->MaxLen,
				'Ff' 			=> @$this->Ff,
				'dimensions' 	=> $dimensions,
				'fontSize' 		=> $this->_parseFontSize(),
				'data' 			=> $d,
			];
		}
	}

	class PDF_Data_Page extends PDF_Data_Type_Base{

		public function __construct($data = null){
			if(!is_null($data)){
				parent::__construct($data);
			}			
		}

		public function htmlData($multiplier = 1){

			/*
			if(!isset($this->image)){
				PdfData::$instance->parseImages();
				$this->image = !PdfData::$instance->_rasterize ? PdfData::$instance->_parsedImages[($this->pdftk_PageNum-1)] : 'data:image/'.\Itul\PdfData\PdfData::$instance->_rasterFormat.';base64,'.base64_encode(file_get_contents(PdfData::$instance->_parsedImages[($this->pdftk_PageNum-1)]));
			}
			*/

			//GET TEXT OBJECTS
			//$texts = [];
			//foreach($this->contents() as $content) foreach($content->texts as $k => $v) $texts[] = $v->htmlData();
			
			//GET FIELD OBJECTS
			$fields = [];
			foreach($this->fields() as $field) $fields[] = $field->htmlData($multiplier);

			return (object)[
				'pageNum' 		=> $this->pdftk_PageNum,
				'dimensions' 	=> $this->dimensions($multiplier, true),
				'fields' 		=> $fields,
				//'text' 			=> $texts,
				//'image' 		=> $this->image,
			];
		}

		//THIS METHOD IS SUPPOSED TO RETURN THE TEXT CONTENT OF THE PAGE WITH THE BOUNDING BOXES. NOT CURRENTLY WORKING
		public function contents(){

			if(isset($this->Contents) && is_array($this->Contents)){
				$res = new PDF_Data_Collection;
				foreach($this->Contents as $content) $res->push($content->get()->getStream($this));
				return $res;
			}
			return new PDF_Data_Collection;
		}

		public function fields(){

			if(isset($this->Annots) && is_object($this->Annots) && get_class($this->Annots) == 'Itul\PdfData\PDF_Data_Resource') return $this->load_resource_array($this->Annots->get()->array);
			return new PDF_Data_Collection;
		}

		public function dimensions($multiplier = 1, $round = false){
			
			$res = (object)[
				'width' 	=> floatval($this->CropBox[2]),
				'height' 	=> floatval($this->CropBox[3]),
				'top' 		=> floatval($this->CropBox[1]),
				'left'		=> floatval($this->CropBox[0]),
				'bottom' 	=> $this->CropBox[3]-$this->CropBox[1],
				'right' 	=> $this->CropBox[2]-$this->CropBox[0],
			];

			return $res;
		}

		public function renderOptions($key = null, $val = null){
			if(!isset($this->_renderOptions)) $this->_renderOptions = PdfData::$instance->_renderOptions;
			if(!is_null($key) && is_array($key)) foreach($key as $k => $v) $this->renderOptions($k, $v);
			elseif(!is_null($key)) $this->_renderOptions[$key] = $val;
			return $this;
		}

		public function htmlPage($prefix = null, $values = [], $withStyle = true){

			$this->renderOptions();

			$page = $this->htmlData();			

			if(PdfData::$instance->_renderOptions['includeImage']){
				if(!isset($page->image)){
					PdfData::$instance->parseImages();
					$page->image = !PdfData::$instance->_rasterize ? PdfData::$instance->_parsedImages[($page->pageNum-1)] : 'data:image/'.\Itul\PdfData\PdfData::$instance->_rasterFormat.';base64,'.base64_encode(file_get_contents(PdfData::$instance->_parsedImages[($page->pageNum-1)]));
				}
			}
			else{
				$page->image = false;
			}

			

			$pageRes['wrapper'] = '<div style="width: '.$page->dimensions->width.'pt; height: '.$page->dimensions->height.'pt;" class="page"></div>';
			if($withStyle && !PdfData::$instance->_pageStylesRendered){
				$pageRes['wrapper'] = PdfData::$instance->_defaultPageStyles."\n".$pageRes['wrapper'];
				PdfData::$instance->_pageStylesRendered = true;
			}

			if($page->image){
				$pageRes['image'] = !PdfData::$instance->_rasterize ? $page->image : '<img style="position:absolute; top: '.$page->dimensions->top.'pt; left: '.$page->dimensions->left.'pt; width: '.$page->dimensions->width.'pt; height: '.$page->dimensions->height.'pt;" src="'.$page->image.'">';
			}
			

			//REGISTER THE RENDER OPTIONS
			//$this->renderOptions();



			//LOOP THROUGH THE FIELDS
			foreach($page->fields as $field){

				//SKIP IF A FIELD TYPE WAS NOT DEFINED
				if(!$field->type) continue;

				//SET THE FIELD VALUE
				$field->value = isset($values[$field->name]) ? $values[$field->name] : null;

				//SET THE FIELD NAME PREFIX
				if(!is_null($prefix)) $field->name = $prefix.'['.$field->name.']';

				//START BUFFERING
				ob_start();

				/*
				if(!PdfData::$instance->_pageStylesRendered){
					echo PdfData::$instance->_defaultPageStyles;
					PdfData::$instance->_pageStylesRendered = true;
				}
				*/

				if(in_array($field->type, ['textarea','text'])) $placeholder = @$this->_renderOptions['placeholder'] ? stripslashes($field->data->TU) : '';

				if($field->type == 'textarea'){
					$style = 'style="position:absolute; overflow:'.(!$field->scrollable ? 'hidden' : 'auto').'; top: '.$field->dimensions->top.'pt; left:'.$field->dimensions->left.'pt; width:'.$field->dimensions->width.'pt; height:'.$field->dimensions->height.'pt;'.rtrim(PdfData::$instance->_styleFontSize($field)).'"';
					echo '<textarea name="'.PdfData::$instance->_generateFieldName($field->name).'" class="pdf-form-field pdf-form-field-textarea" '.rtrim($style);
					if(!empty($placeholder)) echo ' placeholder="'.$placeholder.'"';
					if(!empty($field->MaxLen)) echo ' maxlength="'.$field->MaxLen.'"';
					echo '>'.$field->value.'</textarea>';
				}
				elseif($field->type == 'button'){
					if(!$this->_renderOptions['showButtons']) continue;
					$style = 'style="position:absolute; top: '.$field->dimensions->top.'pt; left:'.$field->dimensions->left.'pt; margin:0pt; padding:0pt;" class="pdf-form-field pdf-form-field-btn" ';
					echo '<button '.$style.'  name="'.PdfData::$instance->_generateFieldName($field->name).'" value="'.$field->value.'">'.stripslashes($field->data->TU).'</button>';

				}
				elseif($field->type == 'checkbox'){
					echo '<label class="checkbox custom-checkbox" style="top:'.$field->dimensions->top.'pt; left:'.$field->dimensions->left.'pt; width:'.$field->dimensions->width.'pt; height:'.$field->dimensions->height.'pt;">';
					echo '<input type="checkbox" name="'.PdfData::$instance->_generateFieldName($field->name).'"';

					//CHECK FOR CONCAT FIELDS
					if(!$field->data->AP){
						$p = explode('_', $field->name);
						array_pop($p);
						$p = implode('_', $p);
						foreach($page->fields as $f) if($f->name == $p) $field->data = $f->data;
					}

					//SET FIELD VALUE
					foreach($field->data->AP as $fieldVals){
						$fKeys = array_keys(json_decode(json_encode($fieldVals), true));
						$fieldValName = trim(array_shift($fKeys));
						echo ' value="'.$fieldValName.'"';
						break;
					}

					//SET CHECKED
					if($field->value) echo ' checked';
					echo '><span class="checkmark"></span></label>';
				}
				else{
					$style = 'style="position:absolute; top:'.$field->dimensions->top.'pt; left:'.$field->dimensions->left.'pt;'.PdfData::$instance->_styleFontSize($field);					
					//echo '<input '.rtrim($style).' width:'.$field->dimensions->width.'pt; height:'.$field->dimensions->height.'pt;"';
					echo '<input type="'.($field->type == 'text' ? 'text' : $field->type).'" name="'.PdfData::$instance->_generateFieldName($field->name).'"';
                    
					

					//IF THIS FIELD IS TEXT
					if($field->type == 'text'){
						echo ' class="pdf-form-field pdf-form-field-text"';
						if(!empty($placeholder)) echo ' placeholder="'.$placeholder.'"';
						if(!empty($field->MaxLen)) echo ' maxlength="'.$field->MaxLen.'"';
						if(!empty($field->value)) echo ' value="'.$field->value.'" ';
					}
					else{
						echo ' class="pdf-form-field"';
					}

                    echo ' '.rtrim($style).' width:'.$field->dimensions->width.'pt; height:'.$field->dimensions->height.'pt;"';
					echo '>';
				}
				
				//GET THE RENDERED FIELDS
				$pageRes['fields'][$field->name] = ob_get_clean();
			}

			//HANDLE FIELD WRAPPING
			if(PdfData::$instance->_fieldWrap) foreach($pageRes['fields'] as $k => $v) $pageRes['fields'][$k] = str_replace('*', $v, PdfData::$instance->_fieldWrap);

			//SEND BACK THE HTML PAGE OBJECT
			return new PDF_Html_Page($pageRes, $this);
		}

		public function render($prefix = null, $values = [], $toString = false, $withStyle = true){			

			return $this->htmlPage($prefix, $values, $withStyle)->render($toString);
		}
	}

	class PDF_Data_Pages extends PDF_Data_Type_Base{

		public function __construct($data){
			parent::__construct($data);
		}
	}

	class PDF_Data_Catalog extends PDF_Data_Type_Base{

		public function __construct($data){
			parent::__construct($data);
		}
	}

	class PDF_Html_Page implements \JsonSerializable{

		public $wrapper;
		public $image;
		public $fields;

		public function __construct($data, $pageObject){

			$this->wrapper 	= $data['wrapper'];
			$this->image 	= @$data['image'] ? $data['image'] : false;
			$this->fields 	= new PDF_Data_Collection($data['fields']);
			$this->data 	= $pageObject;

		}

		public function __toString(){
			return $this->render(true);
		}

		public function jsonSerialize() {
	        return [
	        	'wrapper' 	=> $this->wrapper,
	        	'image' 	=> $this->image,
	        	'fields' 	=> $this->fields,
	        ];
	    }

		//RENDER THE PDF TO HTML
		public function render($string = false){

			//START THE OUTPUT BUFFER
			ob_start();

			//PRINT THE PAGE WRAPPER
			echo substr($this->wrapper, 0, -6)."\n";

			//PRINT THE BACKGROUND IMAGE
			if($this->image) echo "	".$this->image."\n";

			//LOOP TRROUGH THE FIELDS AND PRINT THEM
			foreach($this->fields as $field) echo "	".$field."\n";

			//PRINT THE CLOSING DIV FOR THE WRAPPER
			echo '</div>';

			//GET THE OUTPUT BUFFER AS A STRING
			$res = ob_get_clean();

			//RETURN THE STRING IF NEEDED
			if($string) return $res;

			//PRINT THE STRING
			echo $res;
		}
	}

	class PDF_Data_Collection implements \Iterator, \ArrayAccess, \Countable, \JsonSerializable {

		private $_position 	= 0;
		private $_data 		= array();
		private $_keys 		= [];
		private $_results;

		public function __construct($data = []){
			if(!empty($data)){
				$this->push($data);
				return $this;
			}
		}

		public function __get($name){
			if(substr($name, 0, 1) == '_'){
				$new_name = substr($name, 1);
				if(is_numeric($new_name)){
					return $this->offsetGet($new_name);
				}
			}

			return $this->offsetGet($name);
		}

		public function jsonSerialize() {
	        return $this->_data;
	    }

		public function offsetSet($offset, $value) {
	        if (is_null($offset)) {
	            $this->_data[] = $value;
	        } else {
	            $this->_data[$offset] = $value;
	        }
	        $this->_keys = array_keys($this->_data);
	    }

	    public function offsetExists($offset) {
	        return isset($this->_data[$offset]);
	    }

	    public function offsetUnset($offset) {
	        unset($this->_data[$offset]);
	        $this->_keys = array_keys($this->_data);
	    }

	    public function offsetGet($offset) {
	        return isset($this->_data[$offset]) ? $this->_data[$offset] : null;
	    }

		public function to_array($full = false){

			if($full){
				$res = [];
				foreach($this->_data as $k => $v){
					if(is_object($v)){
						if(is_model($v)) $res[$k] = $v->expose_data();
						elseif(get_class($v) == 'Itul\PdfData\PDF_Data_Collection') $res[$k] = $v->to_array(true);
					}
					else $res[$k] = $v;
				}

				return $res;
			}

			return $this->_data;
		}

		public function count($value = true){
			return count($this->_data);
		}

		public function valid(){
			return isset($this->_keys[$this->_position]);
		}

		public function rewind(){
			$this->_position = 0;
			return $this;
		}

		public function current(){
			return $this->_data[$this->_keys[$this->_position]];
		}

		public function key(){
			return $this->_keys[$this->_position];
		}

		public function nth($offset){
			return $this->offsetGet($offset);
		}

		public function next(){
			$this->_position ++;
			return $this;
		}

		public function previous(){
			if($this->_position > 0) $this->_position --;
			return $this;
		}

		public function first(){
			foreach($this->_data as $item) return $item;
		}

		public function last(){

			$keys = $this->_keys;
			$last_key = array_pop($keys);
			return $this->_data[$last_key];
			
		}

		public function update($key = false, $value){
			if(!$key){
				$key = $this->_position;
			}

			$this->_data[$key] = $value;
			return $this;
		}

		public function make($data){
			if(is_array($data)){
				$this->_data = $data;
			}
			elseif(is_object($data)){
				if(get_class($data) == 'Itul\PdfData\PDF_Data_Collection'){
					$this->_data = $data->to_array();
				}
				elseif(is_model($data)){
					$this->_data[] = $data;
				}
			}

			$this->_keys = array_keys($this->_data);

			return $this;
		}

		public static function collect($data){
			$res = new PDF_Data_Collection;
			return $res->make($data);
		}

		public function push($data){
			if(is_array($data)){
				foreach($data as $k => $v) $this->_data[$k] = $v;
			}
			else{
				$this->_data[] = $data;
			}

			$this->_keys = array_keys($this->_data);

			return $this;
		}
	}